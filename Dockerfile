FROM python:3
MAINTAINER "Wojtek Kaminski"
WORKDIR /usr/src/app
COPY  . .
RUN pip install --no-cache-dir -r requiremants.txt 
EXPOSE 8080
HEALTHCHECK --interval=5m --timeout=5s CMD curl -f http://localhost:8080 || exit 1
CMD ["python", "run.py"]
