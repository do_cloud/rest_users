# Import flask and template operators
from flask import Flask, render_template
# from logging.config import dictConfig
def create_app():
    # Define the WSGI application object
    app = Flask(__name__)
    
    # Configurations
    app.config.from_object('config')
    
    # setup logging
    #dictConfig({
    #    'version': 1,
    #    'formatters': {'default': {
    #        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    #    }},
    #    'handlers': {'wsgi': {
    #        'class': 'logging.StreamHandler',
    #        'stream': 'ext://flask.logging.wsgi_errors_stream',
    #        'formatter': 'default'
    #    }},
    #    'root': {
    #        'level': app.config['LOGGING_LEVEL'],
    #        'handlers': ['wsgi']
    #    }
    #})
    
    # Import a module / component using its blueprint handler variable (mod_auth)
   # from app.rest_users.api import mod_users as mod_users
    
    
    # Register blueprint(s)
    #app.register_blueprint(mod_users)
    return app
