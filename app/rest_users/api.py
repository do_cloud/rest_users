from flask import  request, render_template, \
                flash, g, session, redirect, url_for,abort
from app import create_app
import requests
from flask import jsonify

app = create_app()

users=[
    {
        'id': 1,
        'name': 'wojtek',
        'email': u'wojtek@wojtek.pl', 
        'active': 'true'
    },
    {
        'id': 2,
        'name': 'tomek',
        'email': u'albert@wojtek.pl', 
        'active': 'true'
    }
]



@app.route('/users', methods=['GET'])
def notification_get():
    app.logger.info("getting all users")
    return jsonify(users)
    
    
@app.route('/users/<int:user_id>', methods=['GET'])
def notification_get_id(user_id):
    user = [usr for usr in users if usr['id'] == user_id]
    if len(user) == 0:
        abort(404)
    return jsonify({'user': user[0]})

@app.route('/users', methods=['POST'])
def notification_post():
    error = ''
    app.logger.info("wchodze w post ")
    app.logger.info(request.json)
    users.append(request.json)
    # POST SHOULD RETURN 201
    return jsonify({'created':True}), 201, {'ContentType':'application/json'}

@app.route('/users/<int:user_id>', methods=['PUT'])
def notification_put(user_id):
    user = [usr for usr in users if usr['id'] == user_id]
    if len(user) == 0:
        abort(404)
    user[0]['email']=request.json['email']
    return jsonify({'updated':True}), 200, {'ContentType':'application/json'}
    

@app.route('/users/<int:user_id>', methods=['DELETE'])
def notification_delete(user_id):
    user = [usr for usr in users if usr['id'] == user_id]
    if len(user) == 0:
        abort(404)
    users.remove(user[0])
    return jsonify({'deleted':True}), 200, {'ContentType':'application/json'}