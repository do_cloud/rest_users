# Statement for enabling the development environment
DEBUG = False
# Set up logging level
LOGGING_LEVEL='INFO'
# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  
# Define the database - we are working with
# SQLite for this example
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
# DATABASE_CONNECT_OPTIONS = {}

# Define Connection string to MongoDB
#MONGO_DB_URI='mongodb+srv://airowner:LoveIsInTheAir@air-slxrr.mongodb.net/test?retryWrites=true'

#MONGO_DB_NAME='airpollution'

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "secret"
# Secret key for signing cookies
SECRET_KEY = "secret"
# Configure path to API with air condition data
# AIR_API_URL = "http://api.gios.gov.pl/pjp-api/rest/"