import os
import tempfile

import pytest
#from flask import jsonify

import json
from app.rest_users import api
from flask import jsonify

@pytest.fixture
def client(request):
    test_client = api.app
    return test_client
    
def test_get_all(client):
    c=client.test_client()
    response=c.get('/users')
    assert response.status_code==200

def test_get_one(client):
    c=client.test_client()
    response=c.get("/users/3")
    assert response.status_code!=200

def test_post(client):
    c=client.test_client()
    mimetype = 'application/json'
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }
    data = {
        'id': 100,
        'name': 'wojtek',
        'email': u'wojtek@wojtek.pl', 
        'active': 'true'
    }
    url = '/users'

    response=c.post(url, data=json.dumps(data), headers=headers)
    response_get=c.get("/users/100")
    
    assert response.status_code==201 and response_get.status_code==200
    
    
def test_get_100(client):
    c=client.test_client()
    response=c.get("/users/100")
    print(response.json)
    assert response.status_code==200
